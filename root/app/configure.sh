#! /bin/sh

IFACE=eth0
IFACE_IP=$(ip addr show dev ${IFACE} | grep -oE "\b([0-9]{1,3}\.){3}[0-9]{1,3}\b" | head -1)
HOMEBRIDGE_FLAGS=--insecure

ALL_SWITCHES="$(cat /app/switches1.conf),$(cat /app/switches2.conf),$(cat /app/switches3.conf)"
ALL_SWITCHES=$(echo ${ALL_SWITCHES} |  sed -e "s/,,*/,/g" -e "s/^,//" -e "s/,$//")

cat /app/config.json | sed \
  -e "s/{{BRIDGE_USERNAME}}/${BRIDGE_USERNAME}/g" \
  -e "s/{{BRIDGE_PIN}}/${BRIDGE_PIN}/g" \
  -e "s/{{BRIDGE_SETUPID}}/${BRIDGE_SETUPID}/g" \
  -e "s/{{IFACE_IP}}/${IFACE_IP}/g" \
  -e "s/{{USERNAME}}/${USERNAME}/g" \
  -e "s/{{PASSWORD}}/${PASSWORD}/g" \
  -e "s/{{ALL_SWITCHES}}/${ALL_SWITCHES}/g" \
  > /app/homebridge/config.json
